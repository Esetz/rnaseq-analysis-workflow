'''
This file contains functions and variables shared between several snakefiles,
such as information on samples, assemblies, outputs...
'''


## Imports required python packages
import os
from collections import defaultdict


# Workflow-level usage
## Sets-up global constraints on wildcards:
wildcard_constraints:
    sample = "[-a-zA-Z0-9_.]*",
    assembly = "[-a-zA-Z0-9]*",
    exon_or_gene = "exons|genes"


## Imports species information. This dict contains the locationes to download gtf files and fasta files
assert config.get("assemblies") and os.path.exists(config['assemblies']), \
    "Assemblies description file can't be found. Please check its location and that it corresponds to the value of the 'assemblies' field in config.yaml"
with open(config['assemblies']) as assembly:
    header1 = next(assembly)
    rows = [line.strip('\n').split('\t') for line in assembly if line.strip() != ""]
    assemblies_dict = {row[0]: {'fasta_location': row[1], 'gtf_location': row[2]} for row in rows}


## Imports samples information. This dict contains all the parameters used during the basic analyses: samples names, species, sequencing type
assert config.get("samples") and os.path.exists(config['samples']), \
    "Samples description file can't be found. Please check its location and that it corresponds to the value of the 'samples' field in config.yaml"
with open(config['samples']) as info:
    header2 = next(info)
    rows = [line.strip('\n').split('\t') for line in info if line.strip() != ""]
    samples_dict = {row[0]: {'Location': row[1], 'Assembly': row[2], 'Sequencing_type': row[3]} for row in rows}


## Checks for location of multiqc config file
assert config.get("multiqc") and os.path.exists(config['multiqc']), \
    "MultiQC configuration file can't be found. Please check its location and that it corresponds to the value of the 'multiqc' field in config.yaml"


## Creates a reverse dict with assemblies and their associated samples. Used for count tables and junction files creation
with open(config['samples']) as sample:
    header3 = next(sample)
    rows = [line.strip('\n').split('\t') for line in sample if line.strip() != ""]
    rev_samples_dict = defaultdict(list)
    for row in rows:
        rev_samples_dict[row[2]].append(row[0])
    rev_samples_dict = dict(rev_samples_dict)


## Imports DE contrasts information. This dict contains all the parameters used during DE calculation.
if config.get("contrasts") and os.path.exists(config['contrasts']):
    with open(config['contrasts']) as contrast:
        header4 = next(contrast)
        rows = [line.strip('\n').split('\t') for line in contrast if line.strip() != ""]
        contrasts_list = [{'Assembly': row[0], 'Condition_type': row[1], 'Condition_target': row[2], 'Condition_control': row[3], 'Batch_effect': row[4], 'DE_threshold': row[5], 'aposteriori': row[6]} for row in rows]
        # Creates the filename depending of the presence of DE_threshold and Batch_effect
        for cont in contrasts_list:
            if (cont['DE_threshold'] == '' or cont['DE_threshold'] == '1') and cont['Batch_effect'] == '':
                if cont['aposteriori'] != '':
                    cont['Filename'] = f"{cont['Assembly']}_{cont['Condition_type']}_{cont['Condition_target']}_vs_{cont['Condition_control']}_fc1_aposteriori{cont['aposteriori']}_DE.pdf"
                else:
                    cont['Filename'] = f"{cont['Assembly']}_{cont['Condition_type']}_{cont['Condition_target']}_vs_{cont['Condition_control']}_fc1_aposteriori1.5_DE.pdf"
            elif (cont['DE_threshold'] == '' or cont['DE_threshold'] == '1') and cont['Batch_effect'] != '':
                if cont['aposteriori'] != '':
                    cont['Filename'] = f"{cont['Assembly']}_{cont['Condition_type']}_{cont['Condition_target']}_vs_{cont['Condition_control']}_{cont['Batch_effect']}_fc1_aposteriori{cont['aposteriori']}_DE.pdf"
                else:
                    cont['Filename'] = f"{cont['Assembly']}_{cont['Condition_type']}_{cont['Condition_target']}_vs_{cont['Condition_control']}_{cont['Batch_effect']}_fc1_aposteriori1.5_DE.pdf"
            elif cont['DE_threshold'] != '' and cont['Batch_effect'] == '':
                cont['Filename'] = f"{cont['Assembly']}_{cont['Condition_type']}_{cont['Condition_target']}_vs_{cont['Condition_control']}_fc{cont['DE_threshold']}_DE.pdf"
            else:
                cont['Filename'] = f"{cont['Assembly']}_{cont['Condition_type']}_{cont['Condition_target']}_vs_{cont['Condition_control']}_{cont['Batch_effect']}_fc{cont['DE_threshold']}_DE.pdf"


## Determines the outputs to generate according to the parameters used in config file
def get_outputs(wildcards):
    whole_gnm = [f"results/{sample_data['Assembly']}/{sample}/{sample}_genome_coverage.pdf" for sample, sample_data in samples_dict.items()]
    gene_cov = [f"results/{sample_data['Assembly']}/{sample}/{sample}_genes_coverage_TPM.pdf" for sample, sample_data in samples_dict.items()]
    exon_cov = [f"results/{sample_data['Assembly']}/{sample}/{sample}_exons_coverage_TPM.pdf" for sample, sample_data in samples_dict.items()]
    stats = [f"results/{sample_data['Assembly']}/{sample}/multiqc_report/{sample}_multiqc_report.html" for sample, sample_data in samples_dict.items()]
    count_tables = [f"results/{asmb}/{asmb}_count_table.tsv" for asmb in rev_samples_dict.keys()]
    tpm_tables = [f"results/{asmb}/{asmb}_TPM_table.tsv" for asmb in rev_samples_dict.keys()]
    output = {'whole_gnm': whole_gnm, 'gene_cov': gene_cov, 'exon_cov': exon_cov, 'stats': stats, 'count_tables': count_tables, 'tpm_tables': tpm_tables}
    # If DE is required
    if config.get("contrasts") and os.path.exists(config['contrasts']):
        output['contrasts'] = [os.path.join('results', contrast['Assembly'], 'DE', contrast['Filename']) for contrast in contrasts_list]
    # If bigwig coverage tracks are needed
    if config['bigwig_coverage'] == 'yes':
        output['bigwig_coverage'] = [f"results/{sample_data['Assembly']}/{sample}/{sample}_genome_coverage.bw" for sample, sample_data in samples_dict.items()]
    # If exon junctions are needed
    if config['junctions'] == 'yes':
        output['junctions'] = [f"results/{junc}/{junc}_junctions.bed" for junc in rev_samples_dict.keys()]
    return output


# Rule-level usage
## Used in rule read_mapping
def get_trimmed_fastq(wildcards):
    '''
    This function returns 1 or 2 trimmed fastq as files to map, depending
    on the sample being single-end or paired-end respectively. It also helps
    snakemake to decide which pathway to use for the sample (SE or PE).
    '''
    if samples_dict[wildcards.sample]['Sequencing_type'] == "SE":
        return ['results/{assembly}/{sample}/{sample}_atropos_trimmed.fastq']
    elif samples_dict[wildcards.sample]['Sequencing_type'] == "PE":
        return ['results/{assembly}/{sample}/{sample}_atropos_trimmed_1.fastq', 'results/{assembly}/{sample}/{sample}_atropos_trimmed_2.fastq']


## Used in rules mapping_stats_qualimap, reads_quantification_genes,
## reads_quantification_exons, junctions_coordinates_detection
def get_strandedness(wildcards):
    '''
    This functions returns the strandedness parameter to use in featureCounts,
    Qualimap bamqc and Qualimap rnaseq. A sample is considered as stranded if
    at least 60% of the reads explains a direction.

    The first if test is a place-holder to allow dry-runs and DAG building.
    Otherwise, the function crashes because open() doesn't find the strandedness
    report when the DAG is evaluated, even if all the rules that need this
    function are executed after the rule that produces the report.
    '''
    if not os.path.exists(f"results/{samples_dict[wildcards.sample]['Assembly']}/{wildcards.sample}/{wildcards.sample}_strandedness_report.txt"):
        return [''] * 4
    # Opens strandedness report associated to a specific sample
    with open(f"results/{samples_dict[wildcards.sample]['Assembly']}/{wildcards.sample}/{wildcards.sample}_strandedness_report.txt") as strandedness_report:
        # Reinitializes value
        fail = fwd = 0
        for line in strandedness_report:
            # Saves the proportion of undetermined reads
            if line.startswith("Fraction of reads failed"):
                fail = float(line.strip().split(": ")[1])
            # Saves the proportion of forward reads
            elif line.startswith(('Fraction of reads explained by "1++', 'Fraction of reads explained by "++')):
                fwd = float(line.strip().split(": ")[1])
        # Returns a list of parameter values to use in different softwares:
        # 1st software = featureCounts, 2nd software = Qualimap bamqc,
        # 3rd software = Qualimap rnaseq, 4th software = regtools
        if fwd > 0.6:
            strand = [1, "forward-stranded", "strand-specific-forward", 2]
        elif 1 - (fwd + fail) > 0.6:
            strand = [2, "reverse-stranded", "strand-specific-reverse", 1]
        else:
            strand = [0, "non-strand-specific", "non-strand-specific", 0]
    return strand


## Used in rule count_table
def get_gene_counts(wildcards):
    '''
    This function lists all the gene count tables of an assembly.
    '''
    return [f"results/{wildcards.assembly}/{counts}/{counts}_genes_read_quantification_sorted.tsv" for counts in rev_samples_dict[wildcards.assembly]]


## Used in rule TPM_table
def get_gene_TPM(wildcards):
    '''
    This function lists all the gene TPM tables of an assembly.
    '''
    return [f"results/{wildcards.assembly}/{counts}/{counts}_genes_TPM.tsv" for counts in rev_samples_dict[wildcards.assembly]]


## Used in rule multi_qc
def get_fastqc(wildcards):
    '''
    This function lists the QC files created by fastqc for a sample (2 or 4
    files, depending on the sample being single-end or paired-end respectively).
    '''
    if samples_dict[wildcards.sample]['Sequencing_type'] == "SE":
        return ['results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim.zip', 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim.zip']
    elif samples_dict[wildcards.sample]['Sequencing_type'] == "PE":
        return ['results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim_1.zip', 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim_2.zip',
                'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim_1.zip', 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim_2.zip']


## Used in rule junctions_files_merging
def get_junctions_files(wildcards):
    '''
    This function lists all the junction files for an assembly. There is one
    file for each RNAseq sample processed by the workflow.
    '''
    return [os.path.join('results', wildcards.assembly, junc, junc + '_junctions_coordinates.bed') for junc in rev_samples_dict[wildcards.assembly]]
