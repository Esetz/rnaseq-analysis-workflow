'''
This file contains rules used to download and decompress fasta files (assembly
sequence), fastq files (RNAseq samples) and gtf files (assembly annotations).
It also checks annotation files and fix them when necessary.
'''


# To avoid conflicts during choice of rule to execute
ruleorder: fastq_DL_PE > fastq_DL_SE

rule fastq_DL_PE:
    '''
    If paired-end fastq files are stored locally, this rule will move them to the
    proper folder and decompress them if they're detected as compressed. If the
    files are not stored locally, they will be downloaded from the SRA database.
    '''
    output:
        reads1 = 'results/{assembly}/{sample}/{sample}_1.fastq' if config['fastq'] == "yes" else temp('results/{assembly}/{sample}/{sample}_1.fastq'),
        reads2 = 'results/{assembly}/{sample}/{sample}_2.fastq' if config['fastq'] == "yes" else temp('results/{assembly}/{sample}/{sample}_2.fastq')
    params:
        location = lambda wildcards: samples_dict[wildcards.sample]['Location'],
        tmp = 'results/{assembly}/{sample}/{sample}_tmp'
    log:
        'logs/{assembly}/{sample}/{sample}_fastq_DL.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_fastq_DL.txt'
    conda:
        '../envs/files_DL.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 4096 * attempt,
        runtime = 240
    threads: 8
    shell:
        """
        if [[ -f {params.location}_1.fastq.gz && -f {params.location}_2.fastq.gz ]] && (file {params.location}_1.fastq.gz | grep -q "compressed") && (file {params.location}_2.fastq.gz | grep -q "compressed")
        then
            echo "<{wildcards.sample}> sample fastq files found locally at {params.location}. Files seem to be compressed. Uncompressing to correct directory <results/{wildcards.assembly}/{wildcards.sample}/>" > {log}
            gunzip -c {params.location}_1.fastq.gz > {output.reads1} 2>> {log}
            gunzip -c {params.location}_2.fastq.gz > {output.reads2} 2>> {log}
            echo "Removing compressed files" >> {log}
            rm {params.location}_1.fastq.gz 2>> {log}
            rm {params.location}_2.fastq.gz 2>> {log}
            echo "Done" >> {log}
        elif [[ -f {params.location}_1.fastq && -f {params.location}_2.fastq ]] && (file {params.location}_1.fastq | grep -q "ASCII text") && (file {params.location}_2.fastq | grep -q "ASCII text")
        then
            if [[ {params.location}_1.fastq != {output.reads1} && {params.location}_2.fastq != {output.reads2} ]]
            then
                echo "<{wildcards.sample}> sample fastq files found locally at {params.location}. Files don't seem to be compressed" > {log}
                mv {params.location}_1.fastq {output.reads1} 2>> {log}
                mv {params.location}_2.fastq {output.reads2} 2>> {log}
                echo "<{params.location}_1.fastq> and <{params.location}_2.fastq> moved to <results/{wildcards.assembly}/{wildcards.sample}/>" >> {log}
            elif [[ {params.location}_1.fastq = {output.reads1} && {params.location}_2.fastq != {output.reads2} ]]
            then
                echo "<{params.location}_1.fastq> already in correct directory <results/{wildcards.assembly}/{wildcards.sample}/>" > {log}
                mv {params.location}_2.fastq {output.reads2} 2>> {log}
                echo "<{params.location}_2.fastq> moved to correct directory <results/{wildcards.assembly}/{wildcards.sample}/>" >> {log}
            elif [[ {params.location}_2.fastq = {output.reads2} && {params.location}_1.fastq != {output.reads1} ]]
            then
                echo "<{params.location}_2.fastq> already in correct directory <results/{wildcards.assembly}/{wildcards.sample}/>" > {log}
                mv {params.location}_1.fastq {output.reads1} 2>> {log}
                echo "<{params.location}_1.fastq> moved to correct directory <results/{wildcards.assembly}/{wildcards.sample}/>" >> {log}
            else
                echo "<{params.location}_1.fastq> and <{params.location}_2.fastq> found locally and already in correct directory. Files don't seem to be compressed. Nothing to do" > {log}
            fi
        else
            echo "<{wildcards.sample}> sample fastq files not found locally. Downloading them from SRA at {params.location}" > {log}
            fasterq-dump -e {threads} -x -f -3 --skip-technical -M 25 -t results/{wildcards.assembly}/ -o {params.tmp} {params.location}  &>> {log}
            echo "Moving files to adequate directory" >> {log}
            mv {params.tmp}_1.fastq {output.reads1} 2>> {log}
            mv {params.tmp}_2.fastq {output.reads2} 2>> {log}
            echo "Done" >> {log}
        fi
        """

rule fastq_DL_SE:
    '''
    If a single-end fastq file is stored locally, this rule will move it to the
    proper folder and decompress it if it is detected as compressed. If the file
    is not stored locally, it will be downloaded from the SRA database.
    '''
    output:
        reads = 'results/{assembly}/{sample}/{sample}.fastq' if config['fastq'] == "yes" else temp('results/{assembly}/{sample}/{sample}.fastq')
    params:
        location = lambda wildcards: samples_dict[wildcards.sample]['Location']
    log:
        'logs/{assembly}/{sample}/{sample}_fastq_DL.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_fastq_DL.txt'
    conda:
        '../envs/files_DL.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 4096 * attempt,
        runtime = 240
    threads: 8
    shell:
        """
        if [[ -f {params.location}.fastq.gz ]] && (file {params.location}.fastq.gz | grep -q "compressed")
        then
            echo "<{wildcards.sample}> sample fastq file found locally at {params.location}. File seems to be compressed. Uncompressing to correct directory <results/{wildcards.assembly}/{wildcards.sample}/>" > {log}
            gunzip -c {params.location}.fastq.gz > {output.reads} 2>> {log}
            echo "Removing compressed file" >> {log}
            rm {params.location}.fastq.gz 2>> {log}
            echo "Done" >> {log}
        elif [[ -f {params.location}.fastq ]] && (file {params.location}.fastq | grep -q "ASCII text")
        then
            echo "<{wildcards.sample}> sample fastq file found locally at {params.location}. File doesn't seem to be compressed" > {log}
            mv {params.location}.fastq {output.reads} 2>> {log}
            echo "<{params.location}.fastq> moved to <results/{wildcards.assembly}/{wildcards.sample}/>" >> {log}
        else
            echo "<{wildcards.sample}> sample fastq file not found locally. Downloading it from SRA at {params.location}" > {log}
            fasterq-dump -e {threads} -x -f --skip-technical -M 25 -t results/{wildcards.assembly}/ -o {output.reads} {params.location} &>> {log}
            echo "Done" >> {log}
        fi
        """


rule annotations_DL_unpacking:
    '''
    If an annotation file is stored locally, this rule will move it to the proper
    folder and decompress it if it is detected as compressed. If the file is not
    stored locally, it will be downloaded from the URL provided in the assembly
    description file.
    '''
    output:
        all_gtf = temp('resources/annotations/{assembly}_all_annotations.gtf')
    params:
        temp_gtf = 'resources/annotations/{assembly}',
        gtf_location = lambda wc: assemblies_dict[wc.assembly]['gtf_location']
    log:
        'logs/annotations/{assembly}_annotations_DL.log'
    benchmark:
        'benchmarks/annotations/{assembly}_annotations_DL.txt'
    conda:
        '../envs/files_DL.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 60
    threads: 1
    shell:
        """
        if [[ -f {params.gtf_location} ]] && (file {params.gtf_location} | grep -q compressed)
        then
            echo "{wildcards.assembly} annotation file found locally at {params.gtf_location}. File seems to be compressed. Uncompressing to correct directory <resources/annotations/" > {log}
            gunzip -c {params.gtf_location} > {output.all_gtf} 2>> {log}
            echo "Removing compressed file" >> {log}
            rm {params.gtf_location} 2>> {log}
            echo "Done" >> {log}
        elif [[ -f {params.gtf_location} ]] && (file {params.gtf_location} | grep -q "ASCII text")
        then
            echo "{wildcards.assembly} annotation file found locally at {params.gtf_location}. File doesn't seem to be compressed" > {log}
            mv {params.gtf_location} {output.all_gtf} 2>> {log}
            echo "File {params.gtf_location} moved to <resources/annotations/>" >> {log}
        else
            echo "{wildcards.assembly} annotation file not found locally. Downloading it from source at {params.gtf_location}" > {log}
            rm -f resources/annotations/{wildcards.assembly}_all_annotations.gtf* 2>> {log}  # To avoid any conflict with curl
            curl {params.gtf_location} --output {params.temp_gtf} &>> {log}
            if (file {params.temp_gtf} | grep -q compressed)
            then
                echo "Downloaded file seems to be compressed. Uncompressing to correct directory <resources/annotations/>" >> {log}
                gunzip -c {params.temp_gtf} > {output.all_gtf} 2>> {log}
                echo "Removing compressed file" >> {log}
                rm {params.temp_gtf} 2>> {log}
                echo "Done" >> {log}
            else
                echo "Downloaded file doesn't seem to be compressed" >> {log}
                mv {params.temp_gtf} {output.all_gtf} 2>> {log}
                echo "File {params.temp_gtf} moved to <resources/assembly_files/>" >> {log}
            fi
        fi
        """


rule annotations_checking:
    '''
    If the annotation file uses the gtf format, this rule checks it for empty
    gene_id to avoid FeatureCounts errors and adds pseudogenic transcripts to
    pseudogenes that are missing them.
    '''
    input:
        broken_gtf = rules.annotations_DL_unpacking.output.all_gtf
    output:
        noempty_gtf = temp('resources/annotations/{assembly}_all_annotations_noempty.gtf')
    log:
        'logs/annotations/{assembly}_annotations_checking.log'
    benchmark:
        'benchmarks/annotations/{assembly}_annotations_checking.txt'
    conda:
        '../envs/py.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 2048 * attempt,
        runtime = 30
    threads: 1
    script:
        '../scripts/annotations_checking.py'


rule annotations_fixing:
    '''
    This rule fills empty gene_id in an annotation files to avoid FeatureCounts
    errors and fixes other errors such as missing features, supernumerary
    exons/UTRs...
    '''
    input:
        noempty_gtf = rules.annotations_checking.output.noempty_gtf
    output:
        fixed_gtf = 'resources/annotations/{assembly}_all_annotations_fixed.gtf'
    params:
        almost_fixed_gtf = 'resources/annotations/{assembly}_all_annotations_noempty_agat.gtf'
    log:
        'logs/annotations/{assembly}_annotations_fixing.log'
    benchmark:
        'benchmarks/annotations/{assembly}_annotations_fixing.txt'
    conda:
        '../envs/files_DL.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 8192 * attempt,
        runtime = 120
    threads: 1
    shell:
        """
        echo "Fixing other errors of annotation file" > {log}
        agat_convert_sp_gff2gtf.pl --gff {input.noempty_gtf} --gtf_version 3 --output {params.almost_fixed_gtf} >> {log} 2> /dev/null
        echo "Removing consecutive double-quotes because they make featureCounts crash" >> {log}
        doublequote=$(grep -oh  '" "' {params.almost_fixed_gtf} | wc -l || true)
        if [[ $doublequote == 0 ]]
        then
            echo "There are no consecutive double-quotes in {params.almost_fixed_gtf}" >> {log}
            mv {params.almost_fixed_gtf} {output.fixed_gtf} 2>> {log}  # Changes filename so that workflow continues
        else
            echo "There are ${{doublequote}} consecutive double-quotes in {params.almost_fixed_gtf}. Replacing them by <, >" >> {log}
            sed -E 's/" "/, /g' {params.almost_fixed_gtf} > {output.fixed_gtf} 2>> {log}  # Not using -i because of permission problem
        fi
        echo "Results saved in <{output.fixed_gtf}>" >> {log}
        """


rule gtf2bed:
    '''
    This rule converts gtf files into bed files.
    '''
    input:
        fixed_gtf = rules.annotations_fixing.output.fixed_gtf
    output:
        bed = 'resources/annotations/{assembly}_all_annotations_fixed.bed'
    log:
        'logs/annotations/{assembly}_bed_creation.log'
    benchmark:
        'benchmarks/annotations/{assembly}_bed_creation.txt'
    conda:
        '../envs/files_DL.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 4096 * attempt,
        runtime = 120
    threads: 1
    shell:
        """
        echo "Converting <{input.fixed_gtf}> to bed format" > {log}
        agat_convert_sp_gff2bed.pl --gff {input.fixed_gtf} -o {output.bed} >> {log} 2> /dev/null
        echo "Results saved in <{output.bed}>" >> {log}
        """


rule fasta_DL_unpacking:
    '''
    If a fasta file is stored locally, this rule will move it to the proper
    folder and decompress it if it is detected as compressed. If the file
    is not stored locally, it will be downloaded from the URL provided in
    the assembly description file.
    '''
    output:
        fasta = 'resources/assembly_files/{assembly}_genome.fasta'
    params:
        temp_fasta = 'resources/assembly_files/{assembly}',
        fasta_location = lambda wc: assemblies_dict[wc.assembly]['fasta_location']
    log:
        'logs/assembly_files/{assembly}_fasta_DL.log'
    benchmark:
        'benchmarks/assembly_files/{assembly}_fasta_DL.txt'
    conda:
        '../envs/files_DL.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 60
    threads: 1
    shell:
        """
        if [[ -f {params.fasta_location} ]] && (file {params.fasta_location} | grep -q compressed)
        then
            echo "{wildcards.assembly} assembly file found locally at {params.fasta_location}. File seems to be compressed. Uncompressing to correct directory <resources/assembly_files/>" > {log}
            gunzip -c {params.fasta_location} > {output.fasta} 2>> {log}
            echo "Removing compressed file" >> {log}
            rm {params.fasta_location} 2>> {log}
            echo "Done" >> {log}
        elif [[ -f {params.fasta_location} ]] && (file {params.fasta_location} | grep -q "ASCII text")
        then
            echo "{wildcards.assembly} assembly file found locally at {params.fasta_location}. File doesn't seem to be compressed" > {log}
            mv {params.fasta_location} {output.fasta} 2>> {log}
            echo "File {params.fasta_location} moved to <resources/assembly_files/>" >> {log}
        else
            echo "{wildcards.assembly} assembly file not found locally. Downloading it from source at {params.fasta_location}" > {log}
            rm -f resources/assembly_files/{wildcards.assembly}_genome.fasta* 2>> {log}  # To avoid any conflict with curl
            curl {params.fasta_location} --output {params.temp_fasta} &>> {log}
            if (file {params.temp_fasta} | grep -q compressed)
            then
                echo "Downloaded file seems to be compressed. Uncompressing to correct directory <resources/assembly_files/>" >> {log}
                gunzip -c {params.temp_fasta} > {output.fasta} 2>> {log}
                echo "Removing compressed file" >> {log}
                rm {params.temp_fasta} 2>> {log}
                echo "Done" >> {log}
            else
                echo "Downloaded file doesn't seem to be compressed" >> {log}
                mv {params.temp_fasta} {output.fasta} 2>> {log}
                echo "File {params.temp_fasta} moved to <resources/assembly_files/>" >> {log}
            fi
        fi
        """
