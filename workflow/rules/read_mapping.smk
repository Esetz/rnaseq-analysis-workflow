'''
This file contains rules used to build an index for the reference assembly,
map reads on this assembly, create sorted indexed bam files and read count
tables and find Differentially Expressed Genes (DEGs).
'''


rule index_building:
    '''
    This rule builds a assembly index from an assembly fasta file using Hisat2.
    '''
    input:
        fasta = rules.fasta_DL_unpacking.output.fasta
    output:
        fake_output = 'resources/assembly_files/{assembly}_genome_index_done.txt'
    params:
        index = 'resources/assembly_files/{assembly}_genome_index'
    log:
        'logs/assembly_files/{assembly}_hisat2_index.log'
    benchmark:
        'benchmarks/assembly_files/{assembly}_index_building.txt'
    conda:
        '../envs/read_mapping.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 2048 * attempt,
        runtime = 60
    threads: 8
    shell:
        """
        echo "Using the following command to build <{wildcards.assembly}> genome index:" > {log}
        CMD="hisat2-build -p {threads} -f {input.fasta} {params.index}" 2>> {log}
        echo "${{CMD}}" >> {log}
        echo "Building index" >> {log}
        if ${{CMD}} &>> {log}
        then
            touch {output.fake_output} 2>> {log}
            echo "Index built successfully and saved in <resources/assembly_files/>" >> {log}
        else
            echo "Index building failed. See log for more information" >> {log}
            exit 1
        fi
        """


rule read_mapping:
    '''
    This rule maps trimmed reads of a fastq on a reference assembly.
    '''
    input:
        trim = get_trimmed_fastq,
        fake_output = lambda wildcards: os.path.join('resources/assembly_files', samples_dict[wildcards.sample]['Assembly'] + '_genome_index_done.txt')
    output:
        sam = temp('results/{assembly}/{sample}/{sample}_mapped_reads.sam'),
        report = 'results/{assembly}/{sample}/{sample}_mapping_report.txt',
        metrics = 'results/{assembly}/{sample}/{sample}_mapping_metrics.csv',
        splicesite = 'results/{assembly}/{sample}/{sample}_mapping_novel_splicesite.txt'
    params:
        index = lambda wildcards: os.path.join('resources/assembly_files', samples_dict[wildcards.sample]['Assembly'] + '_genome_index'),
        sq_type = (
            lambda wildcards, input: f"-U {input.trim} --un-bz2 results/{samples_dict[wildcards.sample]['Assembly']}/{wildcards.sample}/{wildcards.sample}_unmapped.fastq.bz2"
            if samples_dict[wildcards.sample]['Sequencing_type'] == "SE"
            else f"-1 {input.trim[0]} -2 {input.trim[1]} --un-conc-bz2 results/{samples_dict[wildcards.sample]['Assembly']}/{wildcards.sample}/{wildcards.sample}_unmapped_%.fastq.bz2"
        )
    log:
        'logs/{assembly}/{sample}/{sample}_mapping.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_mapping.txt'
    conda:
        '../envs/read_mapping.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 4096 * attempt,
        runtime = 300
    threads: 8
    shell:
        """
        echo "Using the following command to map <{wildcards.sample}> reads:" > {log}
        CMD="hisat2 -x {params.index} {params.sq_type} --min-intronlen 20 --max-intronlen 120000 \
        --downstream-transcriptome-assembly -k 10 --max-seeds 10 --fr --no-mixed --no-discordant \
        --time --threads {threads} --novel-splicesite-outfile {output.splicesite} --new-summary \
        --summary-file {output.report} --met-file {output.metrics} --no-unal -S {output.sam}" 2>> {log}
        echo "${{CMD}}" >> {log}
        echo "Mapping the reads" >> {log}
        ${{CMD}} &>> {log}
        echo "Results saved in <{output.sam}>" >> {log}
        """


rule sam_to_bam:
    '''
    This rule converts a sam file to bam format, sorts it and indexes it.
    '''
    input:
        sam = rules.read_mapping.output.sam
    output:
        bam = temp('results/{assembly}/{sample}/{sample}_mapped_reads.bam'),
        bam_sorted = 'results/{assembly}/{sample}/{sample}_mapped_reads_sorted.bam',
        bam_once = temp('results/{assembly}/{sample}/{sample}_mapped_reads_once.bam'),
        bam_once_sorted = 'results/{assembly}/{sample}/{sample}_mapped_reads_once_sorted.bam'
    log:
        'logs/{assembly}/{sample}/{sample}_mapping_sam_to_bam.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_mapping_sam_to_bam.txt'
    conda:
        '../envs/QC.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 12288 * attempt,
        runtime = 240
    threads: 2
    shell:
        """
        echo "Converting <{input.sam}> to BAM format" > {log}
        samtools view -bS {input.sam} --threads {threads} -o {output.bam} 2>> {log}
        echo "Sorting <{output.bam}> file" >> {log}
        samtools sort {output.bam} --threads {threads} -o {output.bam_sorted} 2>> {log}
        echo "Selecting <{input.sam}> reads mapping only once and converting then to BAM format" >> {log}
        samtools view -h -q 60 -bS {input.sam} --threads {threads} -o {output.bam_once} 2>> {log}
        echo "Sorting <{output.bam_once}> file" >> {log}
        samtools sort {output.bam_once} --threads {threads} -o {output.bam_once_sorted} 2>> {log}
        echo "Indexing the sorted BAM file" >> {log}
        samtools index {output.bam_once_sorted} 2>> {log}
        touch {output.bam_once_sorted}.bai
        echo "Results saved in <{output.bam_once_sorted}>" >> {log}
        """


rule strandedness_report:
    '''
    This rules infer the sample strandedness that is essential to get an
    accurate read counting on genes (and the subsequent DE analyses). This
    rule creates a strandedness report that is parsed with get_strandedness().
    '''
    input:
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted,
        bed = rules.gtf2bed.output.bed
    output:
        strandedness_report = 'results/{assembly}/{sample}/{sample}_strandedness_report.txt'
    log:
        'logs/{assembly}/{sample}/{sample}_strandedness_report.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_strandedness_report.txt'
    conda:
        '../envs/read_mapping.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Infering strandedness of <{wildcards.sample}>" > {log}
        infer_experiment.py --input-file {input.bam_once_sorted} --refgene {input.bed} --sample-size 1000000 &> {output.strandedness_report}
        echo "Report can be found in <{output.strandedness_report}>" >> {log}
        """


rule reads_quantification_genes:
    '''
    This rule quantifies the reads of a bam file mapping on genes and produces
    a count table for all genes of the assembly. The strandedness parameter
    is determined by get_strandedness().
    '''
    input:
        feat_annot = lambda wildcards: 'resources/annotations/{assembly}_all_annotations_fixed.gtf',
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted,
        strandedness_report = rules.strandedness_report.output.strandedness_report
    output:
        gene_level = 'results/{assembly}/{sample}/{sample}_genes_read_quantification.tsv',
        gene_summary = 'results/{assembly}/{sample}/{sample}_genes_read_quantification.summary'
    params:
        strandedness = lambda wildcards: get_strandedness(wildcards)[0],
        feature_type = config['feature_type'],
        feat_count = (
            lambda wildcards: [""]
            if samples_dict[wildcards.sample]['Sequencing_type'] == "SE"
            else ["-p -B -C"]
        )
    log:
        'logs/{assembly}/{sample}/{sample}_genes_read_quantification.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_genes_read_quantification.txt'
    conda:
        '../envs/read_mapping.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 60
    threads: 4
    shell:
        """
        echo "Counting reads mapping on genes in <{input.bam_once_sorted}>" > {log}
        featureCounts -a {input.feat_annot} -F GTF -t {params.feature_type} \
        -g gene_id -s {params.strandedness} {params.feat_count} \
        --largestOverlap -T {threads} --verbose -o {output.gene_level} \
        {input.bam_once_sorted} &>> {log}
        echo "Renaming output files" >> {log}
        mv {output.gene_level}.summary {output.gene_summary}
        echo "Results saved in <{output.gene_level}>" >> {log}
        """


rule reads_quantification_exons:
    '''
    This rule quantifies the reads of a bam file mapping on exons and produces
    a count table for all exons of the assembly. The strandedness parameter
    is determined by get_strandedness().
    '''
    input:
        feat_annot = lambda wildcards: 'resources/annotations/{assembly}_all_annotations_fixed.gtf',
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted,
        strandedness_report = rules.strandedness_report.output.strandedness_report
    output:
        exon_level = 'results/{assembly}/{sample}/{sample}_exons_read_quantification.tsv',
        exon_summary = 'results/{assembly}/{sample}/{sample}_exons_read_quantification.summary'
    params:
        strandedness = lambda wildcards: get_strandedness(wildcards)[0],
        feature_type = config['feature_type'],
        feat_count = (
            lambda wildcards: [""]
            if samples_dict[wildcards.sample]['Sequencing_type'] == "SE"
            else ["-p -B -C"]
        )
    log:
        'logs/{assembly}/{sample}/{sample}_exons_read_quantification.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_exons_read_quantification.txt'
    conda:
        '../envs/read_mapping.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 60
    threads: 4
    shell:
        """
        echo "Counting reads mapping on exons in <{input.bam_once_sorted}>" > {log}
        featureCounts -a {input.feat_annot} -F GTF -t {params.feature_type} \
        -g gene_id -f -O --fraction -s {params.strandedness} {params.feat_count} \
        --largestOverlap -T {threads} --verbose -o {output.exon_level} \
        {input.bam_once_sorted} &>> {log}
        echo "Renaming output files" >> {log}
        mv {output.exon_level}.summary {output.exon_summary}
        echo "Results saved in <{output.exon_level}>" >> {log}
        """


rule count_table:
    '''
    This rule merges all the gene count tables of an assembly into one table.
    '''
    input:
        get_gene_counts
    output:
        count_table = 'results/{assembly}/{assembly}_count_table.tsv'
    log:
        'logs/{assembly}/{assembly}_count_table.log'
    benchmark:
        'benchmarks/{assembly}/{assembly}_count_table.txt'
    conda:
        '../envs/py.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    script:
        '../scripts/count_table.py'


rule tpm_table:
    '''
    This rule merges all the gene TPM tables of an assembly into one table.
    '''
    input:
        get_gene_TPM
    output:
        count_table = 'results/{assembly}/{assembly}_TPM_table.tsv'
    log:
        'logs/{assembly}/{assembly}_TPM_table.log'
    benchmark:
        'benchmarks/{assembly}/{assembly}_TPM_table.txt'
    conda:
        '../envs/py.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    script:
        '../scripts/TPM_table.py'


rule deseq2:
    '''
    This rule detects DEGs and plots associated visual control graphs (PCA,
    heatmaps...). The DE comparisons are determined using the information in
    DE_contrasts.tsv.
    '''
    input:
        samples_info = config['samples'],
        count_table = rules.count_table.output.count_table
    wildcard_constraints:
        assembly = '[a-zA-Z0-9\-]*[^_]',
        condition = '[a-zA-Z0-9\-]*[^_]',
        target = '[a-zA-Z0-9\.\-]*[^_]',
        control = '[a-zA-Z0-9\.\-]*[^_]',
        adjust1 = '_|.{0}',
        batch = '[a-zA-Z0-9\-]*[^_]|.{0}',
        threshold = '[0-9\.\-]*[^_]',
        adjust2 = '_|.{0}',
        aposteriori = '[a-zA-Z0-9\.]*[^_]|.{0}'
    output:
        DEG_table = 'results/{assembly}/DE/{assembly}_{condition}_{target}_vs_{control}{adjust1}{batch}_fc{threshold}{adjust2}{aposteriori}_DE_genes.tsv',
        tot_table = 'results/{assembly}/DE/{assembly}_{condition}_{target}_vs_{control}{adjust1}{batch}_fc{threshold}{adjust2}{aposteriori}_DE_total.tsv',
        plots_pdf = 'results/{assembly}/DE/{assembly}_{condition}_{target}_vs_{control}{adjust1}{batch}_fc{threshold}{adjust2}{aposteriori}_DE.pdf'
    params:
        fit_type = config['fit_type'] if 'fit_type' in config else ["parametric"],
        filter = config['filter'] if 'filter' in config else ["T"],
        alpha = config['alpha'] if 'alpha' in config else ["0.05"]
    log:
        'logs/{assembly}/DE/{assembly}_{condition}_{target}_vs_{control}{adjust1}{batch}_fc{threshold}{adjust2}{aposteriori}_DE.log'
    benchmark:
        'benchmarks/{assembly}/DE/{assembly}_{condition}_{target}_vs_{control}{adjust1}{batch}_fc{threshold}{adjust2}{aposteriori}_DE.txt'
    conda:
        '../envs/R.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 4096 * attempt,
        runtime = 60
    threads: 2
    script:
        '../scripts/DESeq2.R'
