'''
Merge gene counts from all samples of an assembly into a single table.
'''


import logging
import os
import pandas as pd
from utils import setup_logging


# Constants
FIELDS = ['Geneid', 'Reads_quant']
STR_TO_REMOVE = '_genes_read_quantification_sorted.tsv'


if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from snakemake')
    list_of_files = snakemake.input
    count_table = snakemake.output.count_table

    output_dir = os.path.dirname(count_table)
    os.makedirs(output_dir, exist_ok=True)

    logging.info(f'Initializing global table with <{list_of_files[0]}>')
    total_table = pd.read_csv(list_of_files[0], sep='\t', index_col=FIELDS[0],
                              usecols=FIELDS)

    for file in list_of_files[1:]:
        logging.info(f'\tAdding data from <{file}>')
        tmp_table = pd.read_csv(file, sep='\t', index_col=FIELDS[0],
                                usecols=FIELDS)
        total_table = pd.concat([total_table, tmp_table], axis=1)

    logging.info('Renaming columns')
    column_titles = [os.path.basename(x).replace(STR_TO_REMOVE, '')
                     for x in list_of_files]
    total_table.columns = column_titles

    logging.info(f'Saving final table in <{count_table}>')
    total_table.to_csv(count_table, sep='\t', header=True, index=True)
    logging.info('Done')
