'''
Count the total number of reads supporting identical junctions.
'''


import logging
import os
import pandas as pd
from utils import setup_logging


# Constants
COL_NAMES_BED = ['Chromosome', 'Junction_start', 'Junction_end',
                 'Junction_name', 'Supporting_reads', 'Strand']
COL_NAMES_SORT = [e for e in COL_NAMES_BED if e not in ('Junction_name',
                                                        'Supporting_reads')]


if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from snakemake')
    merged_bed = snakemake.input.merged_bed
    junctions = snakemake.output.junctions

    output_dir = os.path.dirname(junctions)
    os.makedirs(output_dir, exist_ok=True)

    logging.info(f'Importing merged junctions data from <{merged_bed}>')
    input_bed = pd.read_csv(merged_bed, sep='\t|,', engine='python',
                            names=COL_NAMES_BED)

    logging.info('Summing reads for junction with identical coordinates')
    junctions_bed = input_bed.groupby(COL_NAMES_SORT).sum().reset_index().reindex(columns=input_bed.columns)

    logging.info('Adding standardized names to the junctions')
    logging.info('(JUNC + number of the junction, ex: JUNC42)')
    # Adds 1 to have 1-based numbering
    junctions_bed['Junction_name'] = 'JUNC' + (junctions_bed.index + 1).astype(str)

    logging.info(f'Saving table to <{junctions}>')
    junctions_bed.to_csv(junctions, sep='\t', header=False, index=False)
    logging.info('Done')
